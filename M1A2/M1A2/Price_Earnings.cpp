/*
Kyle Matuszczak / Excelsior IT240 M1A2
v1.1 - 05SEP17

Write a program to prompt the user to enter a company�s earnings-per-share for the year and the price of one 
share of stock.  The program then outputs/display company�s price-to-earnings ratio (price/earnings). 
(Test the program with the amounts $5.25 and $68.25)

Outputs of your program:
1. Company's earnings-per-share.
2. Price of one share of stock.
3. Company's price-to-earnings ratio.
************************************************************************************************************
v1.1 - Replaced 'system("pause");' with 'cin.ignore(); cin.get();' added comments to instructor
************************************************************************************************************
*/
#include <iostream>
#include <iomanip>
using namespace std;

int main() {
	double earnings, share; // i like that i can do multiple variables on single line of matching type
	cout << fixed << setprecision(2); //taken from Ch.3

	cout << "What is the company's earnings per share? (e.g. $5.25): $";
	cin >> earnings;
	cout << "What is the price of a share? (e.g. $68.25): $";
	cin >> share;
	cout << endl;

	cout << "If a company's earnings per share per year is $" << earnings
		<< " and their stock price is $" << share << ",\n"
		<< "then their price to earning ratio is $" << share / earnings << "!\n\n\n"
		<< "Press Enter to close" << endl;
	cin.ignore(); /* It appears that you're testing us on a Mac, so I'm trying to avoid 
				  'system' calls. Learned from stackexchange that my cin.get() was failing 
				  as a pause because the Enter from the previous cin is being passed over. 
				  I'm assuming this is because cin/cout have a buffer, for the same reason
				  I can 'cout << fixed << setprecision(2)' and it holds true through program.
				  Still not sure why your suggested 'cin >> " ";' wouldn't work for me? */
	cin.get();
	return 0;
}